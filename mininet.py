#!/usr/bin/python
# Pedro - 2019

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI
import numpy as np
import shlex, subprocess, os
import time
import sys
import time


# Rodada de teste com apenas alguns jiters, mas com 30 rodadas
tests = 30
results_dir = '~/results/'
link_bandwith = ['100']
link_delay =  ['100']
jitter_s = map(float,np.linspace(0.025, 0.25, 10))
jitter_l = map(float,np.linspace(0.275, 10, 51))
link_jitter = jitter_s + jitter_l
link_jitter.sort()
link_jitter = map(str, link_jitter)
link_jitter.insert(0, '0')
# 0 eh normal, 1 eh uniforme, 2 eh exponential (primeira rodada para uniforme!)
link_distribution = ['0', '1', '2']
tcp_congest_controls = ['bic', 'highspeed', 'westwood', 'yeah']
tcp_sack = ['1']
# nao pode ser zero, pois o calculo do RD utiliza o timestamp
tcp_timestamps = ['1']
tcp_dsack = ['1']


link_fifo = ['5000']
#iperf_tranfer_size = 20971520 #20MB
#iperf_tranfer_size = 67108864 #64MB
iperf_tranfer_size = 125000000 # 375000000 # ~30s de trafego no melhor caso
icmp_test_time = 10
icmp_packet_size = 1518
icmp_file_name = 'pcap_src_10.0.0.1_dst_10.0.0.2-icmp-65535.pcap'


class SingleSwitchTopo(Topo):
    "Single switch connected to n hosts."
    def build(self, n=2):
        switch = self.addSwitch('s1')
        for h in range(n):
            # Each host gets 50%/n of system CPU
            host = self.addHost('h%s' % (h + 1), cpu=.5/n)
            self.addLink(host, switch)
            #self.addLink(host, switch,
            #   bw=100, delay='10ms', loss=0, max_queue_size=1000, use_htb=True)

def createNet():
    topo = SingleSwitchTopo(n=2)
    net = Mininet(topo=topo,
                  host=CPULimitedHost, link=TCLink)
    net.start()
    #net.startTerms()
    print "Dumping host connections"
    dumpNodeConnections(net.hosts)
    return net

def iperfTest(net, output):
    print "Testing bandwidth between h1 and h2"
    h1, h2 = net.get('h1', 'h2')
    my_cmd = 'time iperf -n ' + str(iperf_tranfer_size) + ' -y C -c ' + h2.IP() + ' > ' + results_dir + 'iperf-' + output
    print my_cmd
    # executado no master para guardar info do tcp
    command_line = 'cat /proc/net/tcpprobe'
    args = shlex.split(command_line)
    fo = open(results_dir + 'tcpprobe-' + output, 'w+')
    p = subprocess.Popen(args, bufsize=4096, stdout=fo, stderr=fo)
    # Rodar tcpdump em h2 para capturar o trafego
    tcpdump_file =  results_dir + 'tcpdump_tcp-' + output + '.pcap'
    cmdh2 = 'tcpdump -n -i h2-eth0 -s120 -B 32768 -w ' + tcpdump_file + ' src host 10.0.0.1 and tcp &'
    print cmdh2
    resulth2 = h2.cmd(cmdh2)
    pidh2 = str(int(h2.cmd('echo $!')))
    # Rodar iperf
    result = h1.cmd(my_cmd)
    # Matar tcpdump
    time.sleep(3)
    resulth2 = h2.cmd('kill -15 ', pidh2)
    resulth2 = h2.cmd('wait ', pidh2)
    # calcular tcp_RD
    rd_file = results_dir + 'rd_tcp-' + output
    #rbd_file = results_dir + 'rbd_icmp-' + output
    command_line = "./scripts/tcp_RD.py " + tcpdump_file + " " + rd_file + " 200"
    os.system(command_line)
    # Remover arquivo tcpdump
    command_line = "rm -f " + tcpdump_file
    os.system(command_line)
    print "Finished test."
    #print result
    time.sleep(10)
    # mata o cat e fecha o arquivo
    p.kill()
    print "Closing tcpprobe file"
    fo.close()


def icmpTest(net, bandwidth, output):
    print "Testing icmp sequence test"
    h1, h2 = net.get('h1', 'h2')
    # ajustar o valor de pps (-p) de acordo com o bandwidth
    pps = (int(bandwidth) * 10**6) / (icmp_packet_size * 8)
    # executa o tcpdump em h2. Guarda apenas -s bytes (cabecalhos)
    tcpdump_file =  results_dir + 'tcpdump_icmp-' + output + '.pcap'
    cmdh2 = 'tcpdump -n -i h2-eth0 -s50 -B 32768 -c 65535 -w ' + tcpdump_file + ' &'
    print cmdh2
    resulth2 = h2.cmd(cmdh2)
    pidh2 = str(int(h2.cmd('echo $!')))
    # limitar o envio de pacotes para N segundos
    packet_to_send = pps * icmp_test_time
    tcpreplay_file = '/dev/null'
    cmdh1 = 'tcpreplay -i h1-eth0  -M ' + str(bandwidth)  + ' -L ' + str(packet_to_send) + ' ' + icmp_file_name + ' > ' + tcpreplay_file
    print cmdh1
    # executa o tcpreplay em h1
    resulth1 = h1.cmd(cmdh1)
    print resulth1
    # mata o tcpdump
    print "Going to sleep"
    time.sleep(1)
    print "Waked up!"
    resulth2 = h2.cmd('kill -15 ', pidh2)
    resulth2 = h2.cmd('wait ', pidh2)
    print resulth2
    tshark_file = results_dir + 'tshark_icmp-' + output
    rd_file = results_dir + 'rd_icmp-' + output
    rbd_file = results_dir + 'rbd_icmp-' + output
    #tshark -r tcpdump_icmp-reno-1-1-1-1-1-5-10-1.pcap -T fields -e icmp.seq | ~/RD.pl - 100
    command_line = "tshark -r " + tcpdump_file + " -T fields -e icmp.seq | grep -v '^$' > " + tshark_file
    os.system(command_line)
    #print command_line
    command_line = "rm -f " + tcpdump_file
    os.system(command_line)
    command_line = "./scripts/RDclean.pl " + tshark_file + " 200  > " + rd_file
    os.system(command_line)
    command_line = "./scripts/RBD_Perl_Script_Clean.pl " + tshark_file + " 200  | grep -v '^$' > " + rbd_file
    os.system(command_line)

def do_tests_link(net, test_id, bandwidth, delay, jitter, fifo, distribution):
    s1 = net.get('s1')
    h1 = net.get('h1')
    h2 = net.get('h2')
    savefile = test_id + '-' + bandwidth + '-' + delay + '-' + jitter + '-' + fifo + '-' + distribution
    print "testing link for: ", savefile
    if (int(distribution) == 0):
        # normal
        print "Execute test for normal distribution"
        my_cmd = './scripts/netem-set-djn.sh s1-eth2 ' + bandwidth + ' ' + delay + ' ' + jitter + ' ' + fifo
    if (int(distribution) == 1):
        # uniforme
        print "Execute test for uniform distribution "
        my_cmd = './scripts/netem-set-dju.sh s1-eth2 ' + bandwidth + ' ' + delay + ' ' + jitter + ' ' + fifo
    if (int(distribution) == 2):
        # uniforme
        print "Execute test for exponential distribution "
        my_cmd = './scripts/netem-set-dje.sh s1-eth2 ' + bandwidth + ' ' + delay + ' ' + jitter + ' ' + fifo
    result = s1.cmd(my_cmd)
    print result
    icmpTest(net, bandwidth, savefile)

def do_tests_tcp(net, test_id, tcc, sack, dsack, timestamps, bandwidth, delay, jitter, fifo, distribution):
    s1 = net.get('s1')
    h1 = net.get('h1')
    h2 = net.get('h2')
    savefile = test_id + '-' + bandwidth + '-' + delay + '-' + jitter + '-' + fifo + '-' + distribution + '-' + tcc + '-' + sack + '-' + dsack + '-' + timestamps
    print "testing TCP for: ", savefile
    # trafego sera de h1 (client) para h2 (server)
    # h2 rodarah um servidor, pois se manter um ligado direto tem leak...
    my_cmd = 'iperf -s &> /dev/null &'
    result = h2.cmd(my_cmd)
    pid_iperf = str(int(h2.cmd('echo $!')))
    print "iperf pid server is:", pid_iperf
    iperfTest(net, savefile)
    # agora mata o servidor
    result = h2.cmd('kill -9 ', pid_iperf)
    result = h2.cmd('wait ', pid_iperf)

def split_list(a_list):
    half = len(a_list)/2
    return a_list[:half], a_list[half:]


if __name__ == '__main__':
    split_job = int(sys.argv[1])
    part1, part2 = split_list(link_jitter)

    if split_job == 0:
        link_jitter_split = link_jitter
    elif split_job == 1:
        link_jitter_split = part1
    elif split_job == 2:
        link_jitter_split = part2
    else:
        print "Error - split link delay must be 0, 1 or 2"
        exit()

    print "Partial link_jitter is: ", link_jitter_split

    # coloca valores padroes overall
    command_line = "./scripts/set_default.sh"
    os.system(command_line)
    setLogLevel('info')
    net = createNet()
    # switch onde sera aplicado as regras
    s1 = net.get('s1')
    # cria um regra ligando h1 e h2 para nao sobrecarregar o controller
    my_cmd = "ovs-ofctl add-flow s1 in_port=1,actions=output:2"
    result = s1.cmd(my_cmd)
    my_cmd = "ovs-ofctl add-flow s1 in_port=2,actions=output:1"
    result = s1.cmd(my_cmd)
    my_cmd = "ovs-ofctl dump-flows s1"
    result = s1.cmd(my_cmd)
    print result
    # h2 tera um iperf rodando (server)
    h1 = net.get('h1')
    h2 = net.get('h2')
    #my_cmd = 'iperf -s -D'
    #result = h2.cmd(my_cmd)
    # h2 nao respondera icmp
    my_cmd = 'echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all'
    result = h2.cmd(my_cmd)
    print "Disabling TSO/GSO/UFO"
    my_cmd = "ethtool -K h1-eth0 tso off; /sbin/ethtool -K h1-eth0 gso off; /sbin/ethtool -K h1-eth0 ufo off; "
    result = h1.cmd(my_cmd)
    my_cmd = "ethtool -k h1-eth0 | grep tation-offload;"
    result = h1.cmd(my_cmd)
    print result
    my_cmd = "ethtool -K h2-eth0 tso off; /sbin/ethtool -K h2-eth0 gso off; /sbin/ethtool -K h2-eth0 ufo off;"
    result = h2.cmd(my_cmd)
    my_cmd = "ethtool -k h2-eth0 | grep tation-offload;"
    result = h2.cmd(my_cmd)
    print result
    print "Initial setup done!"

    runs = 0
    for test in range(0, tests):
        for distribution in link_distribution:
            for bandwidth in link_bandwith:
                for delay in link_delay:
                    for fifo in link_fifo:
                        for jitter in link_jitter_split:
                            for tcc in tcp_congest_controls:
                                for sack in tcp_sack:
                                    for dsack in tcp_dsack:
                                        for timestamps in tcp_timestamps:
                                            runs = runs + 1

    print "TOTAL TESTS TO DO IS: ", runs

    for test in range(0, tests):
        test = str(test)
        for distribution in link_distribution:
            for bandwidth in link_bandwith:
                for delay in link_delay:
                    for fifo in link_fifo:
                        for jitter in link_jitter_split:
                            start_time = time.time()
                            do_tests_link(net, test, bandwidth, delay, jitter, fifo, distribution)
                            end_time = time.time()
                            print "link test time: ", end_time - start_time
                            for tcc in tcp_congest_controls:
                                command_line = "echo " + tcc + " > /proc/sys/net/ipv4/tcp_congestion_control"
                                os.system(command_line)
                                for sack in tcp_sack:
                                    command_line = "echo " + sack + " > /proc/sys/net/ipv4/tcp_sack"
                                    os.system(command_line)
                                    for dsack in tcp_dsack:
                                        command_line = "echo " + dsack + " > /proc/sys/net/ipv4/tcp_dsack"
                                        os.system(command_line)
                                        for timestamps in tcp_timestamps:
                                            command_line = "echo " + timestamps + " > /proc/sys/net/ipv4/tcp_timestamps"
                                            os.system(command_line)
                                            start_time = time.time()
                                            do_tests_tcp(net, test, tcc, sack, dsack, timestamps, bandwidth, delay, jitter, fifo, distribution)
                                            end_time = time.time()
                                            print "TCP test time: ", end_time - start_time


    net.stop()
