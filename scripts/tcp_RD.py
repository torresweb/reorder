#!/usr/bin/env python
# arg 1 = arquivo_pcap
# arg 2 = arquivo_saida
# arg 3 = RD Threshold
# recebe como entrada um pcap com dump tcp, onde eh esperado o valor do timestamp
# retorna um arquivo com o RD calculado

import dpkt
import struct
import socket
from subprocess import PIPE, STDOUT
import subprocess, shlex
import sys

filename = sys.argv[1]
#filename='file2.pcap' # 2500*2 em desordem
#filename='file0.pcap' # tudo em ordem
#filename='iperf30s-jitter5.pcap'

RDcommand = './RDclean.pl'
output = sys.argv[2]
RDThreshold = sys.argv[3]
cap = dpkt.pcap.Reader(open(filename,'r'))

def inet_to_str(inet):
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

all_tcp = []
arrive_seq = 1
total_packets = 0
for timestamp, buf in cap:
    total_packets = total_packets + 1
    # Unpack the Ethernet frame (mac src/dst, ethertype)
    eth = dpkt.ethernet.Ethernet(buf)
    # Make sure the Ethernet data contains an IP packet
    if not isinstance(eth.data, dpkt.ip.IP):
        print 'Non IP Packet type not supported %s\n' % eth.data.__class__.__name__
        continue
    # Now grab the data within the Ethernet frame (the IP packet)
    ip = eth.data
    if inet_to_str(ip.src) == '10.0.0.1':
        # Check for TCP in the transport layer
        if isinstance(ip.data, dpkt.tcp.TCP):
            # Set the TCP data
            tcp = ip.data
            tsval = tsecr = 0
            if dpkt.tcp.TCP_OPT_TIMESTAMP:
                for opt in dpkt.tcp.parse_opts(tcp.opts):
                    o, d = opt
                    if o == dpkt.tcp.TCP_OPT_TIMESTAMP:
                        (tsval, tsecr) = struct.unpack('>II', d)
                        seq = "%012d%012d" % (int(tsval), int(tcp.seq))
                        #print seq
                        sent_seq = int(seq)
                        all_tcp.append((arrive_seq, sent_seq))
                        arrive_seq = arrive_seq + 1

# ordena pela ordem de envio
all_tcp = sorted(all_tcp, key=lambda tcp_tuple: tcp_tuple[1])

lista = ''
for i in all_tcp:
    lista = lista + str(i[0]) + '\n'
    #print i[0]
lista = lista[:-1]

command_line = RDcommand + ' - ' + RDThreshold
args = shlex.split(command_line)
fo = open(output, 'w+')
p = subprocess.Popen(args, stdout=fo, stdin=PIPE, stderr=fo)
p.communicate(input=lista)[0]
p.wait()
fo.close()
