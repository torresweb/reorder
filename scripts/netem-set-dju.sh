#!/bin/bash
# Pedro
# Argumentos:
# ${1}: interface
# $2: bandwith Mps,
# $3: delay
# $4: jitter
# $5: fifo limit

echo "Setting Network Emulator"

./scripts/netem-clean.sh ${1}
tc qdisc add dev ${1} handle 1: root htb default 11
tc class add dev ${1} parent 1: classid 1:1 htb rate ${2}Mbps
tc class add dev ${1} parent 1:1 classid 1:11 htb rate ${2}Mbit

if [[ $4 == "0" ]];
then
     tc qdisc add dev ${1} parent 1:11 handle 10: netem limit ${5} delay ${3}ms
else
     tc qdisc add dev ${1} parent 1:11 handle 10: netem limit ${5} delay ${3}ms ${4}ms

fi

tc qdisc
echo "Adjusted tc in ${1}"
