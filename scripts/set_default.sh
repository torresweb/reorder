#!/bin/bash
# Pedro
echo "setting defaults values"

/usr/local/bin/mn -c
echo 1 >  /proc/sys/net/ipv4/tcp_no_metrics_save
killall -9 iperf
killall -9 tcpdump
killall -9 tcpreplay

modprobe -r tcp_probe
modprobe tcp_probe port=5001

echo "setting defaults values - done!"
